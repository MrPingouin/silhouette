/*
	Copyright (c) 2010, Laurent "MrPingouin" CHEA
	All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
	
	- Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
	- Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
	- Neither the name of LAURENTCHEA.COM nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
	THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
	BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
	GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
	STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
	OF SUCH DAMAGE.

*********************************************************************************************************

	MrPingouin's Silhouette Tool 0.1
	contact@laurentchea.com
	http://www.laurentchea.com
	
	dec 2012

	This is a tool for animators.
	It toggles the objects shaders to a pure white, making only their silhouette visible.

*/


g_app_name = "Silhouette"
g_app_version = "0.1"

struct SilhouetteApp 
(
	_keepGrid,
	
	function create =
	(
		local app = SilhouetteApp()
		return app
	),
	
	-- Enable :
	-- Sets all the geometry to vertex channel display, and hides the grid
	
	function enable =
	(
		_keepGrid = viewport.getGridVisibility viewport.activeViewport
		
		if _keepGrid == true then
		(
			viewPort.setGridVisibility viewport.activeViewport false
		)
		
		for o in geometry do
		(
			setCVertMode o true
		)
		
		completeRedraw() -- to redraw viewport immediately, 3dsmax kinda waits a little bit
	),

	function disable	=
	(
		if _keepGrid == true then
		(
			viewPort.setGridVisibility viewport.activeViewport true
		)
		
		for o in geometry do
		(
			setCVertMode o false
		)
		
		completeRedraw() -- to redraw viewport immediately, 3dsmax kinda waits a little bit
	)
	
)

Silhouette_App = SilhouetteApp.create()

------------------
-- UI PART
------------------

rollout SilhouetteRollout ("Silhouette" + " " + g_app_version)
(
	checkButton cb_silhouette "Enable Silhouette"
	
	on cb_silhouette changed state do
	(
		if state == true then
		(
			Silhouette_App.enable()
			cb_silhouette.text = "Disable Silhouette"
		)
		else
		(
			Silhouette_App.disable()
			cb_silhouette.text = "Enable Silhouette"
		)
	)
	
	on SilhouetteRollout close do
	(
		Silhouette_App.disable()
	)
)

createDialog SilhouetteRollout


